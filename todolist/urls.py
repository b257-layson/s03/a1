# from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    # localhost:8000/todolist/1
    path('todoitem/', views.todoitem, name="viewtodoitem"),
    # path('admin/', admin.site.urls),
    # path('todolist/', include('todolist.urls')),
    path('register/', views.register, name="register"),
    path('change-password/', views.change_password, name="change_password"),
    path('login/', views.login_view, name="login"),
    path('logout/', views.logout_view, name="logout")
]
