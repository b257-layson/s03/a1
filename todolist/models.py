from django.db import models

# Create your models here.
class ToDoItem(models.Model):
    # CharField = String value
    # max_length - > is a modifier
    task_name = models.CharField(max_length=50)    
    description = models.CharField(max_length=200)
    status = models.CharField(max_length=50, default="Pending")
    # DateTimeField = DateTime datatype
    date_created = models.DateTimeField("Date Created")